
function yearOfCars(inventory){
    let carYearGrouped={}
    inventory.forEach((car)=>{
        if(!carYearGrouped[car.car_year]){
            carYearGrouped[car.car_year]={}
        }
        carYearGrouped[car.car_year]=car
    })

    let output = Object.entries(carYearGrouped).map(([year, details]) => {
        return { [year]: details };
    });
    return output

}

module.exports=yearOfCars